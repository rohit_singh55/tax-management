# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.6.19)
# Database: tax
# Generation Time: 2014-11-19 13:20:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `payments_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `payment_amount` decimal(12,3) NOT NULL,
  `payment_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payments_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;

INSERT INTO `payment` (`payments_id`, `user_id`, `payment_amount`, `payment_time`)
VALUES
	(1,2,32960.000,'2014-11-16 12:11:48'),
	(2,2,32960.000,'2014-11-17 11:42:18'),
	(3,2,32960.000,'2014-11-18 20:26:33');

/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `questions_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `questionscategory_id` int(11) NOT NULL,
  `questions_text` varchar(255) NOT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `like` int(11) DEFAULT '0',
  `questions_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`questions_id`),
  KEY `user_id` (`user_id`),
  KEY `questionscategory_id` (`questionscategory_id`),
  CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`),
  CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`questionscategory_id`) REFERENCES `questionscategory` (`questionscategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;

INSERT INTO `questions` (`questions_id`, `user_id`, `questionscategory_id`, `questions_text`, `answer`, `like`, `questions_time`)
VALUES
	(1,1,1,'What is my tax.',NULL,0,'2014-11-14 17:34:51'),
	(2,2,1,'What is new tax rate','Calculate It from Website.',0,'2014-11-16 12:41:39'),
	(3,2,1,'my annual income is 7800000',NULL,0,'2014-11-19 10:29:30');

/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table questionscategory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questionscategory`;

CREATE TABLE `questionscategory` (
  `questionscategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionstext` varchar(255) NOT NULL,
  PRIMARY KEY (`questionscategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `questionscategory` WRITE;
/*!40000 ALTER TABLE `questionscategory` DISABLE KEYS */;

INSERT INTO `questionscategory` (`questionscategory_id`, `questionstext`)
VALUES
	(1,'tax'),
	(2,'Financed'),
	(3,'Budget'),
	(4,'Redemption');

/*!40000 ALTER TABLE `questionscategory` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_details`;

CREATE TABLE `user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lasttname` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(45) NOT NULL,
  `admin` varchar(10) NOT NULL,
  `income` decimal(12,3) NOT NULL,
  `category` int(11) NOT NULL,
  `country` varchar(55) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  PRIMARY KEY (`user_id`,`email`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;

INSERT INTO `user_details` (`user_id`, `firstname`, `lasttname`, `email`, `password`, `admin`, `income`, `category`, `country`, `address1`, `address2`, `city`, `state`, `zip`, `mobile`)
VALUES
	(1,'rohit','Singh','dashing_rohit47@yahoo.com','123','user',250000.000,1,'india','93,Girish Ghosh Road','','Kolkata','WB','711202','9903049966'),
	(2,'Sweta','Suman','sweta28@gmail.com','123','user',510000.000,2,'india','howrah','','HOWRAH','WB','711202','9883286574'),
	(3,'Rohit','zas','admin@gmail.com','admin','admin',550000.000,1,'india','howrah','','HOWRAH','west bengal','711202','9903049966');

/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
