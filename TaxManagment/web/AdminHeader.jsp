<%-- 
    Document   : AdminHeader
    Created on : Jul 25, 2014, 8:49:07 PM
    Author     : rohitsingh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
      <title>Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/normalize.css" />
        <link rel="stylesheet" href="css/foundation.css" />
        <!-- If you are using the gem version, you need this only -->
        <link rel="stylesheet" href="css/app.css" />
        <link rel ="stylesheet" href ="global.css">
        <link rel="stylesheet" href="css/customer.css">
      

<%
HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("home.html");
rd.forward(request, response);
}
%>
        <script>
            $(document).foundation();
        </script>

    </head>
    <body>
        <header>
            <div class="row">
                <div class="large-5 columns">
                    <h2><span id="site-name"><a href="home.html"> Tax Management</a></span></h2>

                </div> 
                <div class="large-5 columns">
                    <ul class="inline-list">
                        <li>Hello <% out.println(request.getSession(false).getAttribute("firstname")); %></li>
                        <li><a href="Logout.jsp">Logout</a></li>
                    </ul>
                </div>
            </div>



            <hr>
        </header>
        <div class="content-wrapper">
            <div class="row">
                <div class="large-12 columns">
                    <ul class="inline-list">
                        <li><a href="Payee.jsp">Tax Payers</a></li>
                        <li><a href="AnswerQuestions.jsp">Answer Question</a></li>
                        
                    </ul>
                </div>
            </div>
            <hr>
                    
    </body>
</html>
