/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rohitsingh
 */
@WebServlet(name = "Questions", urlPatterns = {"/Questions"})
public class Questions extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String questiontext=request.getParameter("questiontext");
        String category_id=request.getParameter("category_id");
        int user_id=((Integer) request.getSession(false).getAttribute("user_id")).intValue();
        PrintWriter out = response.getWriter();
        try{
            try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		out.println("Where is your MySQL JDBC Driver?");
		
		return;
	}
 
            String ins="INSERT INTO questions (user_id,questionscategory_id,questions_text) VALUES (?,?,?)";
            
	Connection	connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/tax","root", "root");
                   PreparedStatement preparedStatement=connection.prepareStatement(ins);
         preparedStatement.setInt(1, user_id);
         preparedStatement.setInt(2, Integer.parseInt(category_id));
           preparedStatement.setString(3,questiontext);
       
                                                  
         preparedStatement.executeUpdate();
         out.println("Insetred");
                connection.setAutoCommit(true);
 
         
           }
            catch(Exception e){
        out.print("Error message: "+ e.getMessage()); 
            }
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Questions</title>");            
            out.println("</head>");
            out.println("<body>");
              out.println("<a href=\"Welcome.jsp\">Return</a>");
            out.println("</body>");
            out.println("</html>");
           
        }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
