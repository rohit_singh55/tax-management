/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rohitsingh
 */
public class Log extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
          String email=request.getParameter("email");
      
        String pass=request.getParameter("pass");
         PrintWriter out = response.getWriter();
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Welcome</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> Welcome  " + request.getParameter("email") + "</h1>");
            out.println("</body>");
            out.println("</html>");
        
           try{  
   Class.forName("com.mysql.jdbc.Driver");
          //  String connectionurl="jdbc:sqlserver://u5b42g1a2g.database.windows.net:1433"+";"+"database=ProjectDB"+";"+"user=rohit47@u5b42g1a2g"+";"+"password=@Sumaniya94623"+";"+"encrypt=true"+";"+"hostNameInCertificate=*.database.windows.net"+";"+"loginTimeout=30"+";"+"ssl=require";
      Connection	connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/tax","root", "root");
          
         String select="Select * from user_details where email=? and password=?";
          PreparedStatement preparedStatement=connection.prepareStatement(select);
         
                  
          preparedStatement.setString(1, email);
          preparedStatement.setString(2, pass);
          ResultSet rs=preparedStatement.executeQuery(); 
          if(rs.next()){
            
              
               HttpSession session=request.getSession();
               session.setAttribute("firstname", rs.getString("firstname"));
               session.setAttribute("lastname", rs.getString("lasttname"));
               session.setAttribute("email", rs.getString("email"));
               session.setAttribute("user_id", rs.getInt("user_id"));
               
               out.println(rs.getString("firstname"));
               
               if(rs.getString("admin").equals("user")){
                 
              RequestDispatcher rd=request.getRequestDispatcher("Welcome.jsp");
              rd.forward(request, response);
               }else
               {
                    RequestDispatcher rd=request.getRequestDispatcher("Admin.jsp");
              rd.forward(request, response);
               }
              
          }
          else{
          RequestDispatcher rd=request.getRequestDispatcher("error.jsp");
          //request.
              rd.forward(request, response);
          }
          }
          catch(Exception e){
          
          out.println(e.getMessage());
          }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
