<%-- 
    Document   : Welcome
    Created on : Nov 14, 2014, 3:35:43 PM
    Author     : rohitsingh
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/normalize.css" />
        <link rel="stylesheet" href="css/foundation.css" />
        <!-- If you are using the gem version, you need this only -->
        <link rel="stylesheet" href="css/app.css" />
        <link rel ="stylesheet" href ="global.css">
        <link rel="stylesheet" href="css/customer.css">
        <script src="js/vendor/custom.modernizr.js"></script>
        <script src="js/vendor/jquery.js"></script>
        <script src="js/foundation/foundation.js"></script>
        <script src="js/foundation/foundation.section.js"></script>
        <script src="js/vendor/customer.js"></script>
        <script src="js/foundation/foundation.reveal.js"></script>
        <script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
        <script>
            document.write('<script src=/js/vendor/'
                    + ('__proto__' in {} ? 'zepto' : 'jquery')
                    + '.js><\/script>');
        </script>

<%
HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("home.html");
rd.forward(request, response);
}
%>
        <script>
            $(document).foundation();
        </script>

    </head>
    <body>
        <header>
            <div class="row">
                <div class="large-5 columns">
                    <h2><span id="site-name"><a href="home.html"> Tax Management</a></span></h2>

                </div> 
                <div class="large-5 columns">
                    <ul class="inline-list">
                        <li>Hello <% out.println(request.getSession(false).getAttribute("firstname")); %></li>
                        <li><a href="Logout.jsp">Logout</a></li>
                    </ul>
                </div>
            </div>



            <hr>
        </header>
        <div class="content-wrapper">
            <div class="row">
                <div class="large-12 columns">
                    <ul class="inline-list">
                        <li><a href="AskQuestion.jsp">Create Question</a></li>
                        <li><a href="Activity.jsp">Activities</a></li>
                        <li><a href="CalculateTax.jsp">Calculate Tax</a></li>
                        <li><a href="rate.html">Income Tax Rate</li>
                         <li><a href="vat.html">Vat Calculator</li>
                         <li><a href="salestax.html">Sales Tax Calculator</li
                         
                    </ul>
                </div>
            </div>
            <hr>
          <div class="row">

                <div class="large-12 columns content">
                    <div class="large-12 columns">
                        <h2>Activity</h2>
                    </div>
                     <%
                        
                        ResultSet rs=null;
                         ResultSet rs2=null;
                          ResultSet rs3=null;
                        Connection connection=null;
                        try{
        Class.forName("com.mysql.jdbc.Driver");
          //  String connectionurl="jdbc:sqlserver://u5b42g1a2g.database.windows.net:1433"+";"+"database=ProjectDB"+";"+"user=rohit47@u5b42g1a2g"+";"+"password=@Sumaniya94623"+";"+"encrypt=true"+";"+"hostNameInCertificate=*.database.windows.net"+";"+"loginTimeout=30"+";"+"ssl=require";
       connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/tax","root", "root");
          
         String select="Select * from questions";
          PreparedStatement preparedStatement=connection.prepareStatement(select);
         
                  
        
           rs=preparedStatement.executeQuery(); 
                        }catch(Exception e){}%>
      
                    <div class="large-12 columns">
                        <div class="small-12 large-12 columns">
                                 <% while(rs.next()){%>
                                 <%  try{
                                     String select2="Select * from questionscategory where questionscategory_id = "+rs.getInt(3);
          PreparedStatement preparedStatement2=connection.prepareStatement(select2);
         
                  
        
           rs2=preparedStatement2.executeQuery(); }
                                 catch(Exception e){}
                                 %>
                
                <% try{
                    String select3="Select * from user_details where user_id = "+rs.getInt(2);
          PreparedStatement preparedStatement3=connection.prepareStatement(select3);
         
                  
        
           rs3=preparedStatement3.executeQuery(); }
                catch(Exception e){}%>
          <%while(rs2.next()){ %>
               <%while(rs3.next()){ %>
              
               <p> <b>Asked On <% out.print(rs.getTimestamp(7)); %></b> <% out.print(rs3.getString(2)+" "+rs3.getString(3)); %> has asked question <i> <b>Category : <% out.print(rs2.getString(2)); %> </b> "<% out.print(rs.getString(4)); %> "</p>
               <% if(rs.getString(5)!=null)
               
               {
                   out.println("<p> Answer By Admin : <b>'"+rs.getString(5)+"'</b></p>");

             }%>
                            <hr>
                             <% } %>
                              <% } %>
                            <% } 
                        %>
                         
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <footer>
            <hr>
            <div class="row">

                <div class="large-12 columns">
                    <p> &copy; Pay Tax. All rights reserved.</p>
                </div>
            </div>
        </footer>


    </body>
</html>
