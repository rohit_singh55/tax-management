package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Payment_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <title>Payment</title>\n");
      out.write("            ");

HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("home.html");
rd.forward(request, response);
}

      out.write("\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("       \n");
      out.write("\n");
      out.write("       \n");
      out.write("        <style type=\"text/css\">\n");
      out.write("            #big_wrapper{\n");
      out.write("                text-align: center;\n");
      out.write("                background-color: sienna;\n");
      out.write("                color:white;\n");
      out.write("            }\n");
      out.write("            #link.active{\n");
      out.write("                color:white;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    <title>Shop Online</title>\n");
      out.write("<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"http://cdn.webrupee.com/font\">\n");
      out.write("        <style>\n");
      out.write(" \n");
      out.write("        </style>\n");
      out.write("    <script src=http://cdn.webrupee.com/js type=”text/javascript”></script>\n");
      out.write("    \n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("        \n");
      out.write("        $(function () {\n");
      out.write("  \n");
      out.write("    </script>\n");
      out.write("        <script>\n");
      out.write("$(document).ready(function(){\n");
      out.write("  \n");
      out.write("    $(\"#myimg\").fadeIn();\n");
      out.write("    $(\"#div2\").fadeIn(\"slow\");\n");
      out.write("    $(\"#div3\").fadeIn(10000);\n");
      out.write("    $(\"#flip\").click(function(){\n");
      out.write("    $(\"#panel\").slideDown(\"slow\");\n");
      out.write("  });\n");
      out.write("  \n");
      out.write("});\n");
      out.write("</script>\n");
      out.write("<link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<style>\n");
      out.write("  .first-column,\n");
      out.write("  .column {\n");
      out.write("    float:left;\n");
      out.write("    width:300px;\n");
      out.write("    \n");
      out.write("  }\n");
      out.write("  .column {\n");
      out.write("    margin-left:30px;\n");
      out.write("    margin: 40px;\n");
      out.write("  }\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <center>\n");
      out.write("        <header>\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"large-5 columns\">\n");
      out.write("                    <h2><span id=\"site-name\"><a href=\"home.html\"> Tax Management</a></span></h2>\n");
      out.write("\n");
      out.write("                </div> \n");
      out.write("                <div class=\"large-5 columns\">\n");
      out.write("                    <ul class=\"inline-list\">\n");
      out.write("                       \n");
      out.write("                       \n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            <hr>\n");
      out.write("</header>\n");
      out.write("\n");
      out.write("\n");
      out.write("    \n");
      out.write(" \n");
      out.write("     <div class=\"body-wrapper\">   \n");
      out.write("         <h1>Total Cost <span class=\"WebRupee\">&#x20B9;</span>");
      out.print(request.getSession().getAttribute("taxcalc") );
      out.write("</h1>\n");
      out.write("        \n");
      out.write("        <div id=\"big_wrapper\">\n");
      out.write("          \n");
      out.write("\n");
      out.write("            <form action=\"OrderConfirmation.jsp\" method=\"Post\">                   \n");
      out.write("                <!--div class=\"logmeout\"> <input type=\"submit\" name=\"signout\" value=\" Logout \"> </div-->\n");
      out.write("                <h2> Payment Information </h2> <br /> <br />\n");
      out.write("                Card Type <select name=\"card_type\">\n");
      out.write("                    <option> Mastero </option>   \n");
      out.write("                    <option> VISA </option>  \n");
      out.write("                    <option> Master Card </option>  \n");
      out.write("                    <option> Citi Bank </option>   \n");
      out.write("                    <option> SBI </option>   \n");
      out.write("                    <option> Indian Bank </option>   \n");
      out.write("                </select> <br /> <br />                                         \n");
      out.write("                Card Number <input type=\"text\" value=\"\" maxlength=\"16\" name=\"card_number\" /> <br /> <br />\n");
      out.write("                Name on Card <input type=\"text\" value=\"\" maxlength=\"20\" name=\"card_name\" /> <br /> <br />\n");
      out.write("                Expiration Date <select name=\"expiration_month\">\n");
      out.write("                    <option> 1 </option> <option> 2 </option> <option> 3 </option> <option> 4 </option>   \n");
      out.write("                    <option> 5 </option> <option> 6 </option> <option> 7 </option> <option> 8 </option>   \n");
      out.write("                    <option> 9 </option> <option> 10 </option> <option> 11 </option> <option> 12 </option>   \n");
      out.write("                </select>                      \n");
      out.write("                <select name=\"expiration_year\">\n");
      out.write("                    <option> 2013 </option>   \n");
      out.write("                    <option> 2014 </option>   \n");
      out.write("                    <option> 2015 </option>   \n");
      out.write("                    <option> 2016 </option>   \n");
      out.write("                </select> <br />  <br /> <br />\n");
      out.write("\n");
      out.write("                <h2> Billing Information </h2> <br /> <br /> \n");
      out.write("                Full Name <input type=\"text\" value=\"\" name=\"fullname\" /> <br /> <br />\n");
      out.write("                Address  &nbsp; &nbsp; <input type=\"text\" value=\"\" name=\"address\" /> <br /> <br />\n");
      out.write("                Country  &nbsp; <input type=\"text\" value=\"\" name=\"country\" /> <br /> <br />\n");
      out.write("                Pincode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type=\"text\" value=\"\" name=\"zipcode\" /> <br /> <br />\n");
      out.write("                <input type=\"submit\" name=\"place_order\" />              \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            </form>         \n");
      out.write("        </div>\n");
      out.write("                \n");
      out.write("     </div>\n");
      out.write("</center>\n");
      out.write("              <footer>\n");
      out.write("            <hr>\n");
      out.write("            <div class=\"row\">\n");
      out.write("\n");
      out.write("                <div class=\"large-12 columns\">\n");
      out.write("                    <p> &copy; Pay Tax. All rights reserved.</p>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </footer>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
