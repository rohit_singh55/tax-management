package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.Connection;

public final class CalculateTax_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this template, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<!DOCTYPE html>\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Questions</title>\n");
      out.write("            ");

HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("home.html");
rd.forward(request, response);
}

      out.write("\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/normalize.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/foundation.css\" />\n");
      out.write("        <!-- If you are using the gem version, you need this only -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/app.css\" />\n");
      out.write("        <link rel =\"stylesheet\" href =\"global.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/customer.css\">\n");
      out.write("        <script src=\"js/vendor/custom.modernizr.js\"></script>\n");
      out.write("        <script src=\"js/vendor/jquery.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.section.js\"></script>\n");
      out.write("        <script src=\"js/vendor/customer.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.reveal.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"tinymce/js/tinymce/tinymce.min.js\"></script>\n");
      out.write("        <script>\n");
      out.write("            document.write('<script src=/js/vendor/'\n");
      out.write("                    + ('__proto__' in {} ? 'zepto' : 'jquery')\n");
      out.write("                    + '.js><\\/script>');\n");
      out.write("        </script>\n");
      out.write("\n");
      out.write("\n");
      out.write("        <script>\n");
      out.write("            $(document).foundation();\n");
      out.write("        </script>\n");
      out.write("<script>\n");
      out.write(" function myFunction() {\n");
      out.write("                            var taxamount=\"\";\n");
      out.write("                            var income=document.getElementById(\"income\").value;\n");
      out.write("                            var a=document.getElementById(\"sex\").selectedIndex;\n");
      out.write("                            switch(a){\n");
      out.write("                                case 0:\n");
      out.write("                                    taxamount=income*0.1;\n");
      out.write("                                    document.getElementById(\"ans\").innerHTML=\"Tax Amount \"+taxamount ;\n");
      out.write("                                    break;\n");
      out.write("                                case 1:\n");
      out.write("                                     taxamount=income*0.1;\n");
      out.write("                                     document.getElementById(\"ans\").innerHTML=\"Tax Amount \"+taxamount ;\n");
      out.write("                                    break;\n");
      out.write("                                case 2:\n");
      out.write("                                     taxamount=income*0.2;\n");
      out.write("                                     document.getElementById(\"ans\").innerHTML=\"Tax Amount \"+taxamount ;\n");
      out.write("                                    break;\n");
      out.write("                                case 3:\n");
      out.write("                                     taxamount=income*0.2;\n");
      out.write("                                     document.getElementById(\"ans\").innerHTML=\"Tax Amount \"+taxamount ;\n");
      out.write("                                    break;\n");
      out.write("                            }\n");
      out.write("                            \n");
      out.write("                            \n");
      out.write("                        }</script>\n");
      out.write("    </head>\n");
      out.write("\n");
      out.write("    <body>\n");
      out.write("        <header>\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"large-5 columns\">\n");
      out.write("                    <h2><span id=\"site-name\"><a href=\"home.html\">Tax Management</a></span></h2>\n");
      out.write("\n");
      out.write("                </div> \n");
      out.write("                <div class=\"large-5 columns\">\n");
      out.write("                    <ul class=\"inline-list\">\n");
      out.write("                        <li><a href=\"#\">Hello  ");
 out.println(request.getSession(false).getAttribute("firstname")); 
      out.write("</a></li>\n");
      out.write("                        <li><a href=\"#\">Logout</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            <hr>\n");
      out.write("        </header>\n");
      out.write("        \n");
      out.write("          \n");
      out.write("            <div class=\"row\">\n");
      out.write("\n");
      out.write("                <div class=\"large-12 columns content\">\n");
      out.write("                    <div class=\"large-12 columns\">\n");
      out.write("                       \n");
      out.write("                    </div>\n");
      out.write("                  \n");
      out.write("                        \n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        \n");
      out.write("                        <div class=\"large-10 columns\">\n");
      out.write("\n");
      out.write("                            <label></label><br>\n");
      out.write("                               ");

                        String categry="";
                                double tax=0;
                                double educationtax=0;
                                  double highereducationtax=0;
                             Double in=(Double)request.getSession(false).getAttribute("income");
                                int c=((Integer)request.getSession(false).getAttribute("category")).intValue();
                               
                                
                                switch(c){
                                    case 1:
                                        categry="Male";
                                      if(in<=250000){
                                          
                                      }else if(in>250000&&in<500000){
                                          double t=(in-2500000)*0.1;
                                          tax=t;
                                          
                                          tax+=(tax*0.01)+(tax*0.02);
                                      }else if(in>500000&&in<1000000){
                                      double t1=30000;
                                      double t2=(in-5000000)*0.2;
                                          tax=t1+t2;
                                          educationtax=tax*0.02;
                                          highereducationtax=tax*0.01;
                                          tax+=(educationtax)+(highereducationtax);
                                          
                                      }
                                      else{
                                         double t1=130000;
                                     
                                      double t2=(in-10000000)*0.3;
                                      tax=t1+t2;
                                        educationtax=tax*0.02;
                                          highereducationtax=tax*0.01;
                                          tax+=(educationtax)+(highereducationtax);
                                      }
                                        break;
                                    case 2:
                                        categry="Female";
                                         if(in<=300000){
                                          
                                      }else if(in>300000&&in<500000){
                                          double t=(in-3000000)*0.1;
                                          tax=t;
                                          
                                          tax+=(tax*0.01)+(tax*0.02);
                                      }else if(in>500000&&in<1000000){
                                      double t1=30000;
                                      double t2=(in-500000)*0.2;
                                          tax=t1+t2;
                                          educationtax=tax*0.02;
                                          highereducationtax=tax*0.01;
                                          tax+=(educationtax)+(highereducationtax);
                                          
                                      }
                                      else{
                                         double t1=130000;
                                     
                                      double t2=(in-10000000)*0.3;
                                      tax=t1+t2;
                                        educationtax=tax*0.02;
                                          highereducationtax=tax*0.01;
                                          tax+=(educationtax)+(highereducationtax);
                                      }
                                        
                                        break;
                                    case 3:
                                        categry="Senior Citizen";
                                       categry="Female";
                                         if(in<=300000){
                                          
                                      }else if(in>300000&&in<500000){
                                          double t=(in-3000000)*0.1;
                                          tax=t;
                                          
                                          tax+=(tax*0.01)+(tax*0.02);
                                      }else if(in>500000&&in<1000000){
                                      double t1=30000;
                                      double t2=(in-500000)*0.2;
                                          tax=t1+t2;
                                          educationtax=tax*0.02;
                                          highereducationtax=tax*0.01;
                                          tax+=(educationtax)+(highereducationtax);
                                          
                                      }
                                      else{
                                         double t1=130000;
                                     
                                      double t2=(in-10000000)*0.3;
                                      tax=t1+t2;
                                        educationtax=tax*0.02;
                                          highereducationtax=tax*0.01;
                                          tax+=(educationtax)+(highereducationtax);
                                      }
                                        
                                        
                                        break;
                                    case 4:
                                        categry="Super Senior Citizen";
                                          if(in<=500000){
                                          
                                      }else if(in>500000&&in<1000000){
                                      double t1=30000;
                                      double t2=(in-5000000)*0.2;
                                          tax=t1+t2;
                                          educationtax=tax*0.02;
                                          highereducationtax=tax*0.01;
                                          tax+=(educationtax)+(highereducationtax);
                                          
                                      }
                                      else{
                                         double t1=130000;
                                     
                                      double t2=(in-10000000)*0.3;
                                      tax=t1+t2;
                                        educationtax=tax*0.02;
                                          highereducationtax=tax*0.01;
                                          tax+=(educationtax)+(highereducationtax);
                                      }
                                        
                                        break;
                                        
                                
                                }
                                request.getSession().setAttribute("taxcalc", tax);
                               
      out.write("\n");
      out.write("                               \n");
      out.write("                               <h1>Income Tax Calculator <span class=assesment-year>2015-16</span></h1>\n");
      out.write("<div>\t<div id=share-info-wrapper class=share-info-wrapper data-icon=1></div>\n");
      out.write("</div>\n");
      out.write("<p>Based on your input, the following table gives information about your (");
 out.print(categry); 
      out.write(") taxable income and the total income tax you are supposed to pay in year 2015-16</p>\n");
      out.write("<h4 class=\"text-success t-xxlarge b alert tc\">Your total tax payable is <span class=\"WebRupee\">&#x20B9;</span>  ");
 out.print(tax); 
      out.write("/-</h4>\n");
      out.write("\n");
      out.write("\n");
      out.write("<div class=\"form-horizontal highlight-green pad-x-large b\">\n");
      out.write("<p>Education Cess @ 2% is <span class=\"WebRupee\">&#x20B9;</span>");
 out.print(educationtax); 
      out.write("</p>\n");
      out.write("<p>Secondary and Higher Education Cess @ 1% is <span class=\"WebRupee\">&#x20B9;</span>");
 out.print(highereducationtax); 
      out.write("</p>\n");
      out.write("<p>Surcharge is <span class=\"WebRupee\">&#x20B9;</span>.0</p>\n");
      out.write("<p>Slab Tax is <span class=\"WebRupee\">&#x20B9;</span>");
 out.print((tax-(highereducationtax+educationtax))); 
      out.write("</p>\n");
      out.write("<p class=\"text-success t-large b no-margin\">Your total tax payable is <span class=\"WebRupee\">&#x20B9;</span> ");
 out.print(tax); 
      out.write("</p>\n");
      out.write("</div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                \n");
      out.write("                    <div class=\"row \">\n");
      out.write("                        <div class=\"large-5 columns\" >\n");
      out.write("                         \n");
      out.write("                  ");
 if(tax>0)
                  out.println("<a href='Payment.jsp'>Payment</a>");
                  
      out.write("\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                                \n");
      out.write("\n");
      out.write("                        \n");
      out.write("                          \n");
      out.write("                        \n");
      out.write("\n");
      out.write("\n");
      out.write("                        <div class=\"large-4 columns\">\n");
      out.write("                             \n");
      out.write("                            <p id=\"ans\"></p>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"row \">\n");
      out.write("                       \n");
      out.write("                    <div class=\"row \">\n");
      out.write("\n");
      out.write("                        <div class=\"large-4 columns\"  >\n");
      out.write("                         \n");
      out.write("                                   \n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("                </form>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <footer>\n");
      out.write("            <hr>\n");
      out.write("            <div class=\"row\">\n");
      out.write("\n");
      out.write("                <div class=\"large-12 columns\">\n");
      out.write("                    <p> &copy; Pay Tax. All rights reserved.</p>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </footer>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
