package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;

public final class OrderConfirmation_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("           <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("       \n");
      out.write("        <style type=\"text/css\">\n");
      out.write("            #big_wrapper{\n");
      out.write("                text-align: center;\n");
      out.write("                background-color: sienna;\n");
      out.write("                color:white;\n");
      out.write("            }\n");
      out.write("            #link.active{\n");
      out.write("                color:white;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    <title>Shop Online</title>\n");
      out.write("<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"http://cdn.webrupee.com/font\">\n");
      out.write("        <style>\n");
      out.write(" \n");
      out.write("        </style>\n");
      out.write("    <script src=http://cdn.webrupee.com/js type=”text/javascript”></script>\n");
      out.write("    \n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("        \n");
      out.write("        $(function () {\n");
      out.write("  \n");
      out.write("    </script>\n");
      out.write("        <script>\n");
      out.write("$(document).ready(function(){\n");
      out.write("  \n");
      out.write("    $(\"#myimg\").fadeIn();\n");
      out.write("    $(\"#div2\").fadeIn(\"slow\");\n");
      out.write("    $(\"#div3\").fadeIn(10000);\n");
      out.write("    $(\"#flip\").click(function(){\n");
      out.write("    $(\"#panel\").slideDown(\"slow\");\n");
      out.write("  });\n");
      out.write("  \n");
      out.write("});\n");
      out.write("</script>\n");
      out.write("<link href=\"style/style.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<style>\n");
      out.write("  .first-column,\n");
      out.write("  .column {\n");
      out.write("    float:left;\n");
      out.write("    width:300px;\n");
      out.write("    \n");
      out.write("  }\n");
      out.write("  .column {\n");
      out.write("    margin-left:30px;\n");
      out.write("    margin: 40px;\n");
      out.write("  }\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    ");

    
      try{
               // jdbc:sqlserver://u5b42g1a2g.database.windows.net:1433;database=ProjectDB;user=rohit47@u5b42g1a2g;password={your_password_here};encrypt=true;hostNameInCertificate=*.database.windows.net;loginTimeout=30;
   try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		out.println("Where is your MySQL JDBC Driver?");
		
		return;
	}
 
            String ins="INSERT INTO payment (user_id,payment_amount) VALUES (?,?)";
            
	Connection	connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/tax","root", "root");
                   PreparedStatement preparedStatement=connection.prepareStatement(ins);
         preparedStatement.setInt(1, ((Integer)request.getSession(false).getAttribute("user_id")).intValue());
         preparedStatement.setDouble(2,((Double)request.getSession(false).getAttribute("taxcalc")).doubleValue());


         preparedStatement.executeUpdate();
         out.println("Done");
                connection.setAutoCommit(true);
 
         
           }
            catch(Exception e){
        out.print("Error message: "+ e.getMessage()); 
            }
        
    
    
    
      out.write("\n");
      out.write("<center>\n");
      out.write("  \n");
      out.write("    \n");
      out.write(" \n");
      out.write("     <div class=\"body-wrapper\">   \n");
      out.write("         <h1>Total Tax <span class=\"WebRupee\">&#x20B9;</span>");
      out.print(request.getSession().getAttribute("taxcalc") );
      out.write("</h1>\n");
      out.write("        \n");
      out.write("        <div id=\"big_wrapper\">\n");
      out.write("           \n");
      out.write("       <h1> Payment Successful!!!  <br /> <br /> <br /> \n");
      out.write("                    Thank you for Paying </h1>  <br /> <br /> <br /> <br /> <br />\n");
      out.write("                <div id=\"link\">    <h2> <a href=\"Welcome.jsp\"> Home Page </a> </h2> </div>\n");
      out.write("   \n");
      out.write("        \n");
      out.write("        </div>\n");
      out.write("     </div></center>\n");
      out.write("        </body>\n");
      out.write("    \n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
