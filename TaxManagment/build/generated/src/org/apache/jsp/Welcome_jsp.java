package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.Connection;

public final class Welcome_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this template, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Welcome</title>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/normalize.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/foundation.css\" />\n");
      out.write("        <!-- If you are using the gem version, you need this only -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/app.css\" />\n");
      out.write("        <link rel =\"stylesheet\" href =\"global.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/customer.css\">\n");
      out.write("        <script src=\"js/vendor/custom.modernizr.js\"></script>\n");
      out.write("        <script src=\"js/vendor/jquery.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.section.js\"></script>\n");
      out.write("        <script src=\"js/vendor/customer.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.reveal.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"tinymce/js/tinymce/tinymce.min.js\"></script>\n");
      out.write("        <script>\n");
      out.write("            document.write('<script src=/js/vendor/'\n");
      out.write("                    + ('__proto__' in {} ? 'zepto' : 'jquery')\n");
      out.write("                    + '.js><\\/script>');\n");
      out.write("        </script>\n");
      out.write("\n");

HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("home.html");
rd.forward(request, response);
}

      out.write("\n");
      out.write("        <script>\n");
      out.write("            $(document).foundation();\n");
      out.write("        </script>\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <header>\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"large-5 columns\">\n");
      out.write("                    <h2><span id=\"site-name\"><a href=\"home.html\"> Tax Management</a></span></h2>\n");
      out.write("\n");
      out.write("                </div> \n");
      out.write("                <div class=\"large-5 columns\">\n");
      out.write("                    <ul class=\"inline-list\">\n");
      out.write("                        <li>Hello ");
 out.println(request.getSession(false).getAttribute("firstname")); 
      out.write("</li>\n");
      out.write("                        <li><a href=\"Logout.jsp\">Logout</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            <hr>\n");
      out.write("        </header>\n");
      out.write("        <div class=\"content-wrapper\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"large-12 columns\">\n");
      out.write("                    <ul class=\"inline-list\">\n");
      out.write("                        <li><a href=\"AskQuestion.jsp\">Create Question</a></li>\n");
      out.write("                        <li><a href=\"Activity.jsp\">Activities</a></li>\n");
      out.write("                        <li><a href=\"CalculateTax.jsp\">Calculate Tax</a></li>\n");
      out.write("                        <li><a href=\"rate.html\">Income Tax Rate</li>\n");
      out.write("                         <li><a href=\"vat.html\">Vat Calculator</li>\n");
      out.write("                         <li><a href=\"salestax.html\">Sales Tax Calculator</li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <hr>\n");
      out.write("          <div class=\"row\">\n");
      out.write("\n");
      out.write("                <div class=\"large-12 columns content\">\n");
      out.write("                    <div class=\"large-12 columns\">\n");
      out.write("                        <h2>Activity</h2>\n");
      out.write("                    </div>\n");
      out.write("                     ");

                        
                        ResultSet rs=null;
                         ResultSet rs2=null;
                          ResultSet rs3=null;
                        Connection connection=null;
                        try{
        Class.forName("com.mysql.jdbc.Driver");
          //  String connectionurl="jdbc:sqlserver://u5b42g1a2g.database.windows.net:1433"+";"+"database=ProjectDB"+";"+"user=rohit47@u5b42g1a2g"+";"+"password=@Sumaniya94623"+";"+"encrypt=true"+";"+"hostNameInCertificate=*.database.windows.net"+";"+"loginTimeout=30"+";"+"ssl=require";
       connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/tax","root", "root");
          
         String select="Select * from questions";
          PreparedStatement preparedStatement=connection.prepareStatement(select);
         
                  
        
           rs=preparedStatement.executeQuery(); 
                        }catch(Exception e){}
      out.write("\n");
      out.write("      \n");
      out.write("                    <div class=\"large-12 columns\">\n");
      out.write("                        <div class=\"small-12 large-12 columns\">\n");
      out.write("                                 ");
 while(rs.next()){
      out.write("\n");
      out.write("                                 ");
  try{
                                     String select2="Select * from questionscategory where questionscategory_id = "+rs.getInt(3);
          PreparedStatement preparedStatement2=connection.prepareStatement(select2);
         
                  
        
           rs2=preparedStatement2.executeQuery(); }
                                 catch(Exception e){}
                                 
      out.write("\n");
      out.write("                \n");
      out.write("                ");
 try{
                    String select3="Select * from user_details where user_id = "+rs.getInt(2);
          PreparedStatement preparedStatement3=connection.prepareStatement(select3);
         
                  
        
           rs3=preparedStatement3.executeQuery(); }
                catch(Exception e){}
      out.write("\n");
      out.write("          ");
while(rs2.next()){ 
      out.write("\n");
      out.write("               ");
while(rs3.next()){ 
      out.write("\n");
      out.write("              \n");
      out.write("               <p> <b>Asked On ");
 out.print(rs.getTimestamp(7)); 
      out.write("</b> ");
 out.print(rs3.getString(2)+" "+rs3.getString(3)); 
      out.write(" has asked question <i> <b>Category : ");
 out.print(rs2.getString(2)); 
      out.write(" </b> \"");
 out.print(rs.getString(4)); 
      out.write(" \"</p>\n");
      out.write("               ");
 if(rs.getString(5)!=null)
               
               {
                   out.println("<p> Answer By Admin : <b>'"+rs.getString(5)+"'</b></p>");

             }
      out.write("\n");
      out.write("                            <hr>\n");
      out.write("                             ");
 } 
      out.write("\n");
      out.write("                              ");
 } 
      out.write("\n");
      out.write("                            ");
 } 
                        
      out.write("\n");
      out.write("                         \n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <footer>\n");
      out.write("            <hr>\n");
      out.write("            <div class=\"row\">\n");
      out.write("\n");
      out.write("                <div class=\"large-12 columns\">\n");
      out.write("                    <p> &copy; Pay Tax. All rights reserved.</p>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </footer>\n");
      out.write("\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
