package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

public final class Admin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/AdminHeader.jsp");
    _jspx_dependants.add("/AdminFoter.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("<title> ADMIN PANEL </title>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/normalize.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/foundation.css\" />\n");
      out.write("        <!-- If you are using the gem version, you need this only -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/app.css\" />\n");
      out.write("        <link rel =\"stylesheet\" href =\"global.css\">\n");
      out.write("            <link rel=\"stylesheet\" href=\"css/customer.css\"></link>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("      <title>Page</title>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/normalize.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/foundation.css\" />\n");
      out.write("        <!-- If you are using the gem version, you need this only -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/app.css\" />\n");
      out.write("        <link rel =\"stylesheet\" href =\"global.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/customer.css\">\n");
      out.write("        <script src=\"js/vendor/custom.modernizr.js\"></script>\n");
      out.write("        <script src=\"js/vendor/jquery.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.section.js\"></script>\n");
      out.write("        <script src=\"js/vendor/customer.js\"></script>\n");
      out.write("        <script src=\"js/foundation/foundation.reveal.js\"></script>\n");
      out.write("        <script type=\"text/javascript\" src=\"tinymce/js/tinymce/tinymce.min.js\"></script>\n");
      out.write("        <script>\n");
      out.write("            document.write('<script src=/js/vendor/'\n");
      out.write("                    + ('__proto__' in {} ? 'zepto' : 'jquery')\n");
      out.write("                    + '.js><\\/script>');\n");
      out.write("        </script>\n");
      out.write("\n");

HttpSession gs=request.getSession(false);
if(gs==null){
RequestDispatcher rd=request.getRequestDispatcher("home.html");
rd.forward(request, response);
}

      out.write("\n");
      out.write("        <script>\n");
      out.write("            $(document).foundation();\n");
      out.write("        </script>\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <header>\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"large-5 columns\">\n");
      out.write("                    <h2><span id=\"site-name\"><a href=\"home.html\"> Tax Management</a></span></h2>\n");
      out.write("\n");
      out.write("                </div> \n");
      out.write("                <div class=\"large-5 columns\">\n");
      out.write("                    <ul class=\"inline-list\">\n");
      out.write("                        <li>Hello ");
 out.println(request.getSession(false).getAttribute("firstname")); 
      out.write("</li>\n");
      out.write("                        <li><a href=\"#\">Logout</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            <hr>\n");
      out.write("        </header>\n");
      out.write("        <div class=\"content-wrapper\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"large-12 columns\">\n");
      out.write("                    <ul class=\"inline-list\">\n");
      out.write("                        <li><a href=\"page.html\">Home</a></li>\n");
      out.write("                        <li><a href=\"AskQuestion.jsp\">Create Question</a></li>\n");
      out.write("                        <li><a href=\"activity.html\">activities</a></li>\n");
      out.write("                        <li><a href=\"CalculateTax.jsp\">Calculate Tax</a></li>\n");
      out.write("                        <li><a href=\"rate.html\">Income Tax Rate</li>\n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <hr>\n");
      out.write("                    \n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("    \n");
      out.write("   \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("    \n");
      out.write("             <div class=\"row\">\n");
      out.write("\n");
      out.write("                <div class=\"large-12 columns content\">\n");
      out.write("                    <div class=\"large-12 columns\">\n");
      out.write("                        <h2>Successful Tax Payers</h2>\n");
      out.write("                    </div>\n");
      out.write("                     ");

                        
                        ResultSet rs=null;
                         ResultSet rs2=null;
                          ResultSet rs3=null;
                        Connection connection=null;
                        try{
        Class.forName("com.mysql.jdbc.Driver");
          //  String connectionurl="jdbc:sqlserver://u5b42g1a2g.database.windows.net:1433"+";"+"database=ProjectDB"+";"+"user=rohit47@u5b42g1a2g"+";"+"password=@Sumaniya94623"+";"+"encrypt=true"+";"+"hostNameInCertificate=*.database.windows.net"+";"+"loginTimeout=30"+";"+"ssl=require";
       connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/tax","root", "root");
          
         String select="Select * from payment";
          PreparedStatement preparedStatement=connection.prepareStatement(select);
         
                  
        
           rs=preparedStatement.executeQuery(); 
                        }catch(Exception e){}
      out.write("\n");
      out.write("      \n");
      out.write("                    <div class=\"large-12 columns\">\n");
      out.write("                        <div class=\"small-12 large-12 columns\">\n");
      out.write("                                 ");
 while(rs.next()){
      out.write("\n");
      out.write("                                \n");
      out.write("                \n");
      out.write("                ");
 try{
                    String select3="Select * from user_details where user_id = "+rs.getInt(2);
          PreparedStatement preparedStatement3=connection.prepareStatement(select3);
         
                  
        
           rs3=preparedStatement3.executeQuery(); }
                catch(Exception e){}
      out.write("\n");
      out.write("        \n");
      out.write("               ");
while(rs3.next()){ 
      out.write("\n");
      out.write("              \n");
      out.write("               <p> <b>Payed On ");
 out.print(rs.getTimestamp(4)); 
      out.write("</b> ");
 out.print(rs3.getString(2)+" "+rs3.getString(3)); 
      out.write("  Payed <span class=\"WebRupee\">&#x20B9;</span> ");
 out.print(rs.getDouble(3)); 
      out.write(" </b> </p>\n");
      out.write("                            <hr>\n");
      out.write("                             ");
 } 
      out.write("\n");
      out.write("                              ");
 } 
      out.write("\n");
      out.write("                            \n");
      out.write("                         \n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("                        <footer>\n");
      out.write("\n");
      out.write("\t<div class=\"row\">\n");
      out.write("\n");
      out.write("            <div class=\"large-12 columns\">\n");
      out.write("    ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Questions</title>\n");
      out.write(" \n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/normalize.css\" />\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/foundation.css\" />\n");
      out.write("        <!-- If you are using the gem version, you need this only -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/app.css\" />\n");
      out.write("        <link rel =\"stylesheet\" href =\"global.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/customer.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <footer\n");
      out.write("      <div class=\"row\">\n");
      out.write("\n");
      out.write("                <div class=\"large-12 columns\">\n");
      out.write("                    <p> &copy; Pay Tax. All rights reserved.</p>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </footer>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("</div>\n");
      out.write("</div>\n");
      out.write("    </footer>\n");
      out.write("</div>\t\t\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
